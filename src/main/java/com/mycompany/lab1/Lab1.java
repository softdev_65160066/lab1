/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package com.mycompany.lab1;
import java.util.Scanner;
/**
 *
 * @author informatics
 */
public class Lab1 {
    
        static char[][] T = {{'-','-','-'},{'-','-','-'},{'-','-','-'}};
        static int col,row; 
        static char player =  'O';
        static int Drow = 0;
        public static void main(String[] args) {
           System.out.println("Welcome to OX game");
           System.out.println("trun O player");
            while(true){
                table();
                inputCR();
                if (check() || checkXY()){
                    table();
                    System.out.println("win");
                    break;
                }
                else if (checkDrow()){
                    System.out.println("Drow");
                    break;
    
                }
                Drow=Drow+1;
                switchPlayer();
        
        
            }
        }
    
        public static void inputCR(){
            Scanner kb = new Scanner(System.in);
            int row = kb.nextInt();
            int col = kb.nextInt();
            T[row][col] = player;
        }
    
        public static void table(){      
            for(int i=0;i<3;i++){
                for(int j=0;j<3;j++){
                    System.out.print(T[i][j]);
    
                }
                System.out.println();
            }  
        }
    
        public static void switchPlayer(){
            if (player == 'O') {
                System.out.println("trun X player");
                player = 'X';
            }
            else {
                System.out.println("trun O player");
                player = 'O';
            }
    
    
        }
                
        public static boolean checkX() {
            for (int c = 0; c < 3; c++) {
                if (T[row][c] != player) {
                    return false;
         
                }
    
            }
            return true;
        }
            public static boolean checkY() {
                for (int r = 0; r < 3; r++) {
                    if (T[r][col] != player) {
                        return false ;
             
                    }
                }
                return true;
            }
    
        public static boolean check() {
            if(checkX()) return true;
            if(checkY()) return true ;
    
            return false;
        }
        public static boolean checkXY(){
                if (T[0][0] == player && T[1][1] == player && T[2][2] == player) {
                    return true ;
         
                }
                else if(T[0][2] == player && T[1][1] == player && T[2][0] == player) {
                    return true ;
         
                }
                else {
                    return false;
                }
    
        }
        public static boolean checkDrow(){
            if (Drow ==8){
                return true ;
         
            }
    
            return false;
            
        }
    }
    
    